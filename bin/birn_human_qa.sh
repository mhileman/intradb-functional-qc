#!/bin/bash
set -e -x

#Given a params file and an index of the scan as input argument,
#this script would generate the BIRN Stats for the scan

SESSION=$1
SCAN_ID=$2
BUILD_DIR=$3
# my_sge_task_id=$1
# paramsFile=$2
QC_DIR=/nrgpackages/tools.release/intradb/functional-qc

source ${QC_DIR}/setup/bxh_xcede_tools-1.10.3_setup.sh
BXH_BIN=$BXH_XCEDE_TOOLS_HOME/bin
source ${QC_DIR}/setup/bxh_xcede_tools_setup.sh;

indir=${BUILD_DIR}/DICOM/${SCAN_ID}
birndir=${BUILD_DIR}/birn
mkdir -p $birndir

derivedDataQA="${birndir}/${SCAN_ID}"
bxhOutputFile=${birndir}/${SCAN_ID}_image.xml
seriesFullPath="${bxhOutputFile}:h"
seriesLabel="${seriesFullPath}:t"

pushd ${indir}
  # dicom2bxh --xcede  *.dcm $birndir/${SCAN_ID}_image.xml
  # Not all DICOMs are coming in with .dcm extension
  dicom2bxh --xcede * $birndir/${SCAN_ID}_image.xml
popd

#############################################################################
#  run analysis script
#############################################################################

date=`date`

$BXH_BIN/fmriqa_generate.pl $bxhOutputFile --overwrite --qalabel $SESSION":Scan"$SCAN_ID "$derivedDataQA"

exit 0
