#!/bin/bash
set -x -e

#Given a params file and an index of the scan as input argument,
#this script would generate the FourierStatistics for the scan

SESSION=$1
SCAN_ID=$2
BUILD_DIR=$3
# paramsFile=$2
#Pass yes or no for isStructural
isStructural=no
QC_DIR=/nrgpackages/tools.release/intradb/functional-qc

# source $paramsFile
source /nrgpackages/scripts/epd-python_setup.sh

fourier_dir=$BUILD_DIR/fourier/$SCAN_ID
indir=${BUILD_DIR}/NIFTI/${SCAN_ID}
inpattern="*.nii.gz"

pushd $indir
python $QC_DIR/bin/FourierSlope.py -D ${indir} -N $inpattern -O $fourier_dir
popd


plot_functional=$QC_DIR/bin/generate_plots_functional.csh
plot_structural=$QC_DIR/bin/generate_plots_struc.csh

mkdir -p $fourier_dir

pushd ${fourier_dir}
	if [ $isStructural = no ] ; then
	  $plot_functional $SESSION $fourier_dir $fourier_dir $SCAN_ID
	else
	  $plot_structural $SESSION $fourier_dir $fourier_dir $SCAN_ID
	fi
popd

exit 0
