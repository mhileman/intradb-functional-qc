#!/bin/bash
set -x -e

#Given a params file and an index of the scan as input argument,
#this script would generate the MotionOutlier Stats for the scan

SESSION=$1
SCAN_ID=$2
BUILD_DIR=$3
# my_sge_task_id=$1
# paramsFile=$2
QC_DIR=/nrgpackages/tools.release/intradb/functional-qc
# source $paramsFile

indir=${BUILD_DIR}/NIFTI/${SCAN_ID}
motion_dir=$BUILD_DIR/motion/$SCAN_ID
temp_dir=$BUILD_DIR/motion/temp

mkdir -p $temp_dir

pushd ${indir}
  $QC_DIR/bin/motion_qc.sh *nii.gz $indir $motion_dir $motion_dir/${SCAN_ID}_mo.dat $temp_dir
popd

exit 0
