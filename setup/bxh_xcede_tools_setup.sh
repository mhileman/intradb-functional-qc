#!/bin/bash

PACKAGES_HOME=/nrgpackages
source ${PACKAGES_HOME}/scripts/AFNI_setup.sh
BXH_XCEDE_TOOLS_HOME=${PACKAGES_HOME}/tools/bxh_xcede_tools
PATH=${BXH_XCEDE_TOOLS_HOME}/bin:${PATH}
export PATH
