#!/bin/bash
set -e -x

USER=$1
PASS=$2
SCAN_URI=$3
SESSION=$4
SCAN=$5
RESOURCE=$6
BUILD_DIR=$7
JSESSIONID=$8

uri=$HOST/xapi/pipelineControlPanel/project/$PROJECT/pipeline/FUNCTIONAL_QC/entity/$SESSION/group/ALL/setValues?status=RUNNING
curl -f -s -k --cookie "JSESSIONID=$JSESSIONID" $uri -X POST -H "Content-Type: application/json"

# Set up build space
zip_dir=$BUILD_DIR/zips
unzip_dir=$BUILD_DIR/$RESOURCE/$SCAN
mkdir -p $zip_dir
mkdir -p $unzip_dir

# uri=$HOST/data/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DICOM/files?format=zip
uri=$SCAN_URI/resources/$RESOURCE/files?format=zip
zip_file=${SESSION}_${SCAN}_${RESOURCE}.zip
curl -s -k -u $USER:$PASS -w "%{http_code}" $uri > $zip_dir/$zip_file
# curl -f -s -k --cookie "JSESSIONID=$JSESSIONID" $uri > $zip_dir/$zip_file
unzip -o -j $zip_dir/$zip_file -d $unzip_dir
