import os
import sys
import threading

# Add path for shared modules
sys.path.append("/nrgpackages/tools.release/intradb")

from IntradbPipeline.params import parser
from IntradbPipeline.common import Pipeline
from hcpxnat.interface import HcpInterface

'''
Set up optparse and HcpInterface first, then use them to instantiate Pipeline.
'''

# TODO check usability

# Add params specific to this pipeline here
# Other parameters are pulled from the generic params.py in IntradbPipeline
# parser.remove_option("execute")
(opts, args) = parser.parse_args()

xnat = HcpInterface(
    url=opts.hostname, username=opts.alias, password=opts.secret)
xnat.project = opts.project
xnat.session_label = opts.session
# Take an optional subject label, otherwise find the subject label
if not opts.subject:
    xnat.subject_label = xnat.getSessionSubject()

# Setup Pipeline object to use common pipeline functionality
pipe = Pipeline(xnat, opts, name='fncqc')

QC_TYPES = ('tfMRI', 'rfMRI')


def main():
    scans = getFunctionalScans()

    handleExistingQc()

    runFunctionalQc(scans)

    pipe.combineScanLogs()
    # shutil.rmtree(build_dir)


def getFunctionalScans():
    scan_ids = set()
    pipe.log.info("Finding scans requiring Functional QC -- {types}".format(
        types=QC_TYPES,))

    # Process only user provided scans if any
    scans_param = opts.scans.split(',') if opts.scans else []
    # Allow 'all' for param for handling by XNAT automation service
    if 'all' in scans_param:
        scans_param.remove('all')

    # TODO check for usability
    for scan in xnat.getSessionScans():
        # xnat.scan_id = scan['ID']

        # Only look at user supplied scan list
        if scans_param and scan['ID'] not in scans_param:
            continue

        st = scan['type']

        for scan_type in QC_TYPES:
            if scan_type in st and not \
              (st.endswith('Ref') or st.endswith('Log') or st.endswith('Physio')):
                scan_ids.add(scan['ID'])

    pipe.log.info("Found {n} scans needing QC. ({scans})".format(
        n=len(scan_ids), scans=','.join(scan_ids))
    )

    # Make sure NIFTI files exist
    for scan in scan_ids:
        xnat.scan_id = scan
        resources = xnat.getScanResources()
        has_nifti = False

        for r in resources:
            if r['label'] == 'NIFTI':
                has_nifti = True

        if not has_nifti:
            pipe.log.error("Scan {} doesn't have NIFTI resource.".format(scan))
            pipe.log.error("Need to run DICOM to NIFTI pipeline.")
            # sys.exit(1)

    return list(scan_ids)


def handleExistingQc():
    assessments = xnat.getSessionAssessors()
    existing_assessments = []

    for ass in assessments:
        if 'qcAssessment' in ass['xsiType']:
            existing_assessments.append(ass)

    if existing_assessments:
        if opts.existing == 'skip':
            pipe.log.warn("Skipping since there's already Functional QC.")
            sys.exit(0)
        elif opts.existing == 'fail':
            pipe.log.error("Existing QC and set to FAIL. Exiting ...")
            sys.exit(1)
        elif opts.existing == 'overwrite':
            pipe.log.info("Overwriting existing QC for all scans.")
            for qc_ass in existing_assessments:
                xnat.deleteSessionAssessor(qc_ass['label'])


def runFunctionalQc(scans):
    pipe.setPcpStatus('FUNCTIONAL_QC', 'QUEUED')

    threads = []

    for scan in scans:
        tname = 'qc_' + str(scan)
        t = threading.Thread(name=tname, target=qcWorker, args=(scan,))
        threads.append(t)
        t.start()

    for t in threads:
        t.join()

    pipe.setPcpEndStatus('FUNCTIONAL_QC')


def qcWorker(scan):
    getImageResources(scan)
    qcScan(scan)
    createAssessors(scan)


def getImageResources(scan):
    pipe.log.info("Getting scan resources for {}".format(scan))
    jsessionid = xnat.get('/REST/JSESSIONID').text

    uri = "{}/data/projects/{}/subjects/{}/experiments/{}/scans/{}".format(
        xnat.url,
        xnat.project,
        xnat.subject_label,
        xnat.session_label,
        scan
    )
    pwd = os.path.dirname(os.path.realpath(__file__))
    script_path = os.path.join(pwd, 'get-resource-files.sh')
    resources = ['DICOM', 'NIFTI']
    threads = []

    for resource in resources:
        cmd = "{} {} {} {} {} {} {} {} {}".format(
            script_path,
            xnat.username,
            xnat.password,
            uri,
            xnat.session_label,
            scan,
            resource,
            pipe.build_dir,
            jsessionid
        )
        jobname = "qcGet{}{}".format(resource, scan)
        t = threading.Thread(
            name=scan,
            target=pipe.submit,
            args=(scan, cmd, jobname, 'hcp_standard.q',)
        )
        threads.append(t)
        t.start()

    # Wait for all threads to finish
    for t in threads:
        t.join()


def qcScan(scan):
    # Run Fourier, Wavelet, Motion, and BIRN statistics all in parallel
    pwd = os.path.dirname(os.path.realpath(__file__))
    scripts = []
    # Per Mike Harms on 2017-3-10, no one uses fourier or wavelet
    # scripts.append(os.path.join(pwd, 'bin', 'fourier-stats.sh'))
    # scripts.append(os.path.join(pwd, 'bin', 'wavelet-stats.sh'))
    # scripts.append(os.path.join(pwd, 'bin', 'motion_qc_wrapper.sh'))
    # Bypass motion_qc_wrapper and call directly
    scripts.append(os.path.join(pwd, 'bin', 'motion_qc.sh'))
    scripts.append(os.path.join(pwd, 'bin', 'birn_human_qa.sh'))
    threads = []

    xnat.scan_id = scan
    series_desc = xnat.getScanXmlElement("xnat:series_description")
    temp_dir = os.path.join(pipe.build_dir, 'motion', 'temp')
    pipe.mkdir_p(temp_dir)

    # Motion QC
    script = os.path.join(pwd, 'bin', 'motion_qc.sh')
    script_base = os.path.basename(script)
    pipe.log.info("Running {} on scan {}".format(script_base, scan))
    cmd = "{script} {task} {indir} {mcdir} {outfile} {tmpdir}".format(
        script=script,
        task="{}_{}.nii.gz".format(xnat.session_label, series_desc),
        indir=os.path.join(pipe.build_dir, 'NIFTI', scan),
        mcdir=os.path.join(pipe.build_dir, 'motion', scan),
        outfile=os.path.join(pipe.build_dir, 'motion', scan,
                             "{}_mo.dat".format(scan)),
        tmpdir=temp_dir
    )
    jobname = "qc{task}{scan}".format(
        task=os.path.basename(script).split('_')[0].upper(),
        scan=scan
    )
    t = threading.Thread(
        name=scan,
        target=pipe.submit,
        args=(scan, cmd, jobname, 'hcp_standard.q')
    )
    threads.append(t)
    t.start()

    # BIRN Human QC
    script = os.path.join(pwd, 'bin', 'birn_human_qa.sh')
    script_base = os.path.basename(script)
    pipe.log.info("Running {} on scan {}".format(script_base, scan))
    cmd = "{script} {session} {scan} {build}".format(
        script=script,
        session=xnat.session_label,
        scan=scan,
        build=pipe.build_dir
    )
    jobname = "qc{task}{scan}".format(
        task=os.path.basename(script).split('_')[0].upper(),
        scan=scan
    )
    t = threading.Thread(
        name=scan,
        target=pipe.submit,
        args=(scan, cmd, jobname, 'hcp_standard.q')
    )
    threads.append(t)
    t.start()

    # Wait for all to complete
    for t in threads:
        t.join()


def createAssessors(scan):
    pipe.log.info("Creating QC Assessment for scan {}".format(scan))
    pwd = os.path.dirname(os.path.realpath(__file__))
    script_path = os.path.join(pwd, 'create-qc-assessor.sh')

    uri = "/data/experiments?xsiType=xnat:mrSessionData&project={}&label={}".format(
        xnat.project, xnat.session_label)
    exp_id = xnat.getJson(uri)[0]['ID']
    jobname = "qcASSES{}".format(scan)

    cmd = "{script} {h} {u} {p} {pr} {su} {se} {eid} {scn} {build}".format(
        script=script_path,
        h=xnat.url,
        u=xnat.username,
        pr=xnat.project,
        p=xnat.password,
        su=xnat.subject_label,
        se=xnat.session_label,
        eid=exp_id,
        scn=scan,
        build=pipe.build_dir
    )

    pipe.submit(scan, cmd, jobname, 'hcp_standard.q')


if __name__ == "__main__":
    main()
